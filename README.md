Testing Web And API

This is the project website testing, API testing on the page url: http://ec2-13-228-39-1.ap-southeast-1.compute.amazonaws.com/

The main task are:

- Write funcional test cases on the website
- Write API test cases
- Write test script and workflow application in postman
- Record the errors found in the file Defect log
- Collet and evaluate results

Deploy on local, clone project:

- with https:
  git clone https://gitlab.com/thiennmfx22887/testingwebandapi.git
- with ssh:
  git clone git@gitlab.com:thiennmfx22887/testingwebandapi.git
- with file json request having postman on laptop
  click new > import file (import all 3 file .json enter collections)

Must skills knowledge:

- Requirement analysis
- Write test case, logbug
- Knowledge basic postmain, workflow, testscripts
